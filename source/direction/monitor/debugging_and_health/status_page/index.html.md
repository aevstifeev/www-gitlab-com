---
layout: markdown_page
title: "Category Vision - Status Page"
---

- TOC
{:toc}

## Status Page

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity | [Planned](/direction/maturity/) |

### Introduction and how you can help
Thanks for visiting this category strategy page on Status Page in GitLab. This page belongs to the [Health](/handbook/product/categories/#health-group) group of the Monitor stage and is maintained by Sarah Waldner [E-Mail](mailto:<swaldner@gitlab.com>).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AStatus%20Page) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AStatus%20Page) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for a status page, we'd especially love to hear from you.

### Overview
A common need for many companies is to communicate the status of a software service that they provide. This could be to internal users, customers, or the general public. These will be collectively referred to as **Stakeholders**. A typical Status Page publicly lists services that it provides and the **status** of that service. It's main purpose is for Stakeholders to self-serve information while internal teams work to restore services that have been disrupted. In the event there is an outage, the Status Page also provides explanation for the outages well as any known timelines.  Additionally, Stakeholders have the option of subscribing to different services for changes in Status.

In an age where systems must be [highly available](https://en.wikipedia.org/wiki/High_availability) and uptime is guaranteed on the order of [six 9's](https://en.wikipedia.org/wiki/High_availability#%22Nines%22), it's crucial that time and effort are not wasted updating Stakeholders in multiple ways.

<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

#### Target Audience
A Status Page needs to make it simple to understand the state of services someone is a Stakeholder of and it must be easy to manage and update internally. The target audience of Status Page can be divided into the following categories:

[Users](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#user-personas) are in charge of maintaining the critical services that their stakeholders depend upon. They are responsible for updating the Status Page during outages.
* [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)


[Buyers](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas) manage the teams who maintain the software services the company provides to customers.

**Consumers** are the Stakeholders that care about the availability of provided software services. These individuals can be internal or external, individual contributors, management, or customers.

#### Challenges to address
We are just beginning research cycles to validate the need and desire for a Status Page as part of GitLab's product offering in the Monitor Stage.

Once we have begun research efforts, it will be documented [here](https://gitlab.com/gitlab-org/gitlab/issues/35430).

### Where we are headed
Our vision is to build a portal for maintainers and Stakeholders that is simple to update, easy to consume, and has a customizable UI. A mature Status Page contains a list of services provided, description, and status as well as the ability for stakeholders to subscribe to notifications on services as there are changes.

GitLab is in a unique position to offer compelling value here:
1. We can generate alerts based on a wide array of observability data: [metrics](https://docs.gitlab.com/ee/administration/monitoring/prometheus/gitlab_metrics.html#gitlab-prometheus-metrics), [traces](https://docs.gitlab.com/ee/user/project/operations/tracing.html), and [logs](https://docs.gitlab.com/ee/user/project/clusters/kubernetes_pod_logs.html#kubernetes-pod-logs-ultimate).
1. We can consume alerts from any monitoring tool via our [generic alert endpoint](https://docs.gitlab.com/ee/user/project/integrations/generic_alerts.html#generic-alerts-integration-ultimate)
1. [We are adding comprehensive Incident Management in 12.6](https://gitlab.com/groups/gitlab-org/-/epics/349)
1. GitLab has a [decentralized runner architecture](https://docs.gitlab.com/ee/ci/runners/#configuring-gitlab-runners) which can run tests from across the world (or via a potential SaaS service) to detect reachability problems
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
#### What's Next & Why
We can start down this road by:
* Build a public page to surface this information cleanly, similar to GitLab Pages but able to deliver live data like charts and incident information
* Integrate with Incident Management, to allow an Ops person to acknowledge and incident and publish status. (If unacknowledged for X minutes, perhaps auto-publish.)
* Once published, make it easy to update directly from the incident.
* Display an availability chart for each service.
* Depending on demand, consider providing a SaaS service for the status page hosting as well as worldwide blackbox monitoring services

#### What is Not Planned Right Now
Scope of work and timeline for Status Page has not been determined this time (11/01/2019). After further research, we will populate this section with functionality that is out of scope.

#### Maturity Plan
This category is currently at the Planned maturity level, and our next maturity target is Minimal (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). Please check out the [MVC for Status Page](https://gitlab.com/gitlab-org/gitlab/issues/6134).

### User success metrics
We will know we are on the right trajectory for Status Page when we are able to observe the following changes. Please note that these metrics are based on our vision for the product and may change as we start working on it.

* GitLab can replace the it's [current status page hosted on status.io](https://status.gitlab.com/) (i.e. we are [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding))
* Increase in status update subscribers
* Increase in the number of services a given customer integrates into the page

### Why is this important?
Managing stakeholders is an important part of the job for any DevOps engineer and a Status Page has become a critical part of the services a software company provides to their customers. It provides a public portal for broad communication during an outage. It helps to reduce the number of related support tickets that come pouring in or the negative blasts on social media. Updating a single portal is much faster than responding to individual inquiries from each customer. This is a natural addition to the GitLab product suite on top of our Incident Management offering. It eliminates yet another integration and ____.

At this time, we have not completed due diligence to confirm this is the most pressing functionality to add to the Monitor Stage. Further research and validation is required to confirm that we will be investing in a GitLab hosted **Status Page**. A fully featured Status Page would likely take 4-5 milestones to build.  
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
## Competitive Landscape
* [Statuspage](www.statuspage.io)
* [status.io](status.io)
* [Cachet](cachethq.io)

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.
